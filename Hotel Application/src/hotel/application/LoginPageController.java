/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application;

import Model.Staff;
import hotel.application.database.StaffQuery;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author renazhou
 */
public class LoginPageController implements Initializable {
    
    @FXML
    private Label staffIdLabel;
    
    @FXML
    private Label passwordLabel;
    
    @FXML
    private TextField staffIdTextField;
    
    @FXML
    private PasswordField passwordPasswordField;
    
    @FXML
    private Button logInButton;
    

    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        String staffId = staffIdTextField.getText();
        String password = passwordPasswordField.getText();
        
        StaffQuery sq = new StaffQuery();
  
      LoginPage lp1 = new LoginPage();

      
        if (sq.login(staffId, password) == true) {
            System.out.println("correct pw"); 


            //get current row of database, setLoggedIn = true and pass it to updatemethod
           // staff.setLoggedin(true);


            //get current row of database, setLoggedIn = true and pass it to updatemethod
           // staff.setLoggedin(true);
          

            //get current row of database, setLoggedIn = true and pass it to updatemethod
           // staff.setLoggedin(true);

            try {
                Parent root = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
                Scene scene = new Scene(root);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(scene);
                app_stage.show();
              
            } catch (Exception e) {
                
            }
            
        } else {
            lp1.wrongPassword();

        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
