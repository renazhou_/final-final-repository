/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Guest;
import Model.Purchase;
import Model.Reservation;
import hotel.application.Purchases;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;

/**
 *
 * @author renazhou
 */
public class GuestQuery extends DatabaseQuery {


    PreparedStatement insertGuest = null; 
    PreparedStatement updateGuest = null;
    PreparedStatement getAllGuests = null;
    PreparedStatement deleteGuest = null;
    
    ResultSet rs = null;

    public List<Guest> getGuest() { 

        List<Guest> guests = new ArrayList<>();
        openConnection();
        try {
            getAllGuests = conn.prepareStatement("select * from app.GUEST");
            rs = getAllGuests.executeQuery();
            while (rs.next()) {
                Guest newGuest = new Guest (rs.getInt("GUESTID"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("email"), rs.getString("address"), rs.getString("phone"));
                System.out.println(newGuest);
                guests.add(newGuest);
                
            }
            rs.close();
            getAllGuests.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();

        return guests;
    }

    public void insertGuest(Guest g) {
        System.out.println("inserting guest entry");

        openConnection();
        try {
            insertGuest = conn.prepareStatement("insert into app.GUEST (firstname, lastname, email, address, phone) values (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);//
            //insertGuest.setInt(1, g.getGuestID());
            insertGuest.setString(1, g.getFname());
            insertGuest.setString(2, g.getLname());
            insertGuest.setString(3, g.getEmail());
            insertGuest.setString(4, g.getAddress());
            insertGuest.setString(5, g.getPhone());

            insertGuest.executeUpdate();

            ResultSet rs = insertGuest.getGeneratedKeys();
            rs.next();
            g.setGuestId(rs.getInt(1));
            
            
            
            if (rs != null) {
                rs.close();
            }
            insertGuest.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }
    
        public void updateGuest(Guest c) {

        System.out.println("Updating first name to: "+c.getFname());
        System.out.println("For id = "+c.getGuestID());
        openConnection();
        try {

            updateGuest = conn.prepareStatement("update app.GUEST set firstname=?, lastname=?, email=?, address=?, phone=? where guestID=?");
            
            updateGuest.setString(1, c.getFname());
            updateGuest.setString(2, c.getLname());
            updateGuest.setString(3, c.getEmail());
            updateGuest.setString(4, c.getAddress());
            updateGuest.setString(5, c.getPhone());
            updateGuest.setInt(6, c.getGuestID());


            updateGuest.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            updateGuest.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
        
        GuestQuery guestQuery = new GuestQuery();
        for(Guest c1 : guestQuery.getGuest()) {
            System.out.println(c1);
        }
    }
    


    public List<Integer> getGuestIDs() {
        List<Integer> guestIDs = new ArrayList<Integer>();
        for (Guest g : this.getGuest()) {
        guestIDs.add(g.getGuestID());
        }
        return guestIDs;
    }
    
    public void deleteGuest(Guest c) {
        System.out.println("deleting Guest");
        openConnection();
        try {

            deleteGuest = conn.prepareStatement("delete from app.GUEST where guestID = ?");
            deleteGuest.setInt(1, c.getGuestID());

            deleteGuest.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            deleteGuest.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }
    
    
}
