/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hotel.application.database;

import Model.Purchase;
import Model.Reservation;
import hotel.application.Purchases;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;


/**
 *
 * @author renazhou
 */
public class PurchasesQuery extends DatabaseQuery{
    PreparedStatement insertPurchase = null; 
    PreparedStatement getAllPurchases = null;
    ResultSet rs = null;

    public List<Purchase> getPurchases() { 
        System.out.println("Getting purchases");
        List<Purchase> purchases = new ArrayList<>();
        openConnection();
        try {
            getAllPurchases = conn.prepareStatement("select * from app.PURCHASES");
            rs = getAllPurchases.executeQuery();
            while (rs.next()) {
               purchases.add(
                     new Purchase (rs.getInt("purchaseQuantity"), rs.getString("purchaseDesc"), rs.getDouble("purchaseCost"))
                );
            }
            rs.close();
            getAllPurchases.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        System.out.println("returning " + purchases.size() + " purchases");
        return purchases;
    }

    public void insertPurchase(Purchase p) {
        System.out.println("Inserting purchase");

        openConnection();
        try {

            insertPurchase = conn.prepareStatement("insert into app.PURCHASES (purchaseQuantity, purchaseDesc, purchaseCost) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            insertPurchase.setInt(1, p.getPurchaseQuantity());
            insertPurchase.setString(2, p.getPurchaseDesc());
            insertPurchase.setDouble(3, p.getPurchaseCost());
            insertPurchase.executeUpdate();
            
            if(rs != null)
                rs.close();
            insertPurchase.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
    }    
    
    
 
    public void initialize(URL url, ResourceBundle rb) {

    }   
}
