/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.application.database;

import Model.Purchase;
import Model.Reservation;
import Model.Room;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;

/**
 *
 * @author renazhou
 */
public class ReservationQuery extends DatabaseQuery {

    private static Object reservation;
    PreparedStatement insertReservation = null;
    PreparedStatement updateReservation = null;
    PreparedStatement deleteReservation = null;
    PreparedStatement getAllReservation = null;
    PreparedStatement getAllBookedRooms = null;
    PreparedStatement getEarlyCheckInAvailability = null;

    ResultSet rs = null;

    public List<Reservation> getReservation() {
        System.out.println("Getting reservation");
        List<Reservation> reservations = new ArrayList<>();
        openConnection();
        try {
            getAllReservation = conn.prepareStatement("select * from app.RESERVATION");
            rs = getAllReservation.executeQuery();
            while (rs.next()) {
                reservations.add(
                        new Reservation(rs.getInt("ID"),rs.getString("refCode"), rs.getInt("guestID"), rs.getString("numPeople"), rs.getString("roomNumber"), rs.getDate("checkIn"), rs.getDate("checkOut"), rs.getString("adjustedCheck"), rs.getString("breakfastdates"), rs.getInt("deposit"), rs.getInt("cost"))
                );
            }
            rs.close();
            getAllReservation.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        System.out.println("returning " + reservations.size() + " reservations");
        return reservations;
    }

    public void insertReservation(Reservation r) {
        System.out.println("inserting res");

        openConnection();
        try {

            insertReservation = conn.prepareStatement("insert into app.RESERVATION (refCode, guestID, numPeople, roomNumber, checkIn, checkOut, adjustedCheck, breakfastDates, deposit, cost, owing) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);//
            insertReservation.setString(1, r.getRefCode());
            insertReservation.setInt(2, r.getGuestID());
            insertReservation.setString(3, r.getNumPeople());
            insertReservation.setString(4, r.getRoomNumber());
            insertReservation.setDate(5, r.getCheckIn());
            insertReservation.setDate(6, r.getCheckOut());
            insertReservation.setString(7, r.getAdjustedCheck());
            insertReservation.setString(8, r.getFreeBreakfast());
            insertReservation.setInt(9, r.getDepositPaid());
            insertReservation.setInt(10, r.getResCost());
            insertReservation.setInt(11, r.getAmountOwing());

            insertReservation.executeUpdate();

            ResultSet rs = insertReservation.getGeneratedKeys();
            rs.next();
            r.setId(rs.getInt(1));
            System.out.println("Resrvation id = " + r.getID());

            if (rs != null) {
                rs.close();
            }
            insertReservation.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }

    public void updateReservation(Reservation r) {

        System.out.println("Updating room number to: " + r.getRoomNumber());
        System.out.println("For id = " + r.getID());
        openConnection();
        try {

            updateReservation = conn.prepareStatement("update app.RESERVATION set guestID=?, numPeople=?, roomNumber=?, checkIn=?, checkOut=?, adjustedCheck=?, breakfastDates=?, deposit=?, cost=? where id=?");
            updateReservation.setInt(1, r.getGuestID());
            updateReservation.setString(2, r.getNumPeople());
            updateReservation.setString(3, r.getRoomNumber());
            updateReservation.setDate(4, r.getCheckIn());
            updateReservation.setDate(5, r.getCheckOut());
            updateReservation.setString(6, r.getAdjustedCheck());
            updateReservation.setString(7, r.getFreeBreakfast());
            updateReservation.setInt(8, r.getDepositPaid());
            updateReservation.setInt(9, r.getResCost());
            updateReservation.setInt(10, r.getID());

            updateReservation.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            updateReservation.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();

        ReservationQuery reservationQuery = new ReservationQuery();
        for (Reservation r1 : reservationQuery.getReservation()) {
            System.out.println(r1);
        }
    }

    public void deleteReservation(Reservation r) {

        openConnection();
        try {

            deleteReservation = conn.prepareStatement("delete from app.RESERVATION where id = ?");
            deleteReservation.setInt(1, r.getID());

            deleteReservation.executeUpdate();

            if (rs != null) {
                rs.close();
            }
            deleteReservation.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        closeConnection();
    }

    public List<String> getBookedRooms(Date startDate, Date endDate) {
        List<String> roomNumbers = new ArrayList<>();
        openConnection();
        try {
            getAllBookedRooms = conn.prepareStatement("select DISTINCT roomNumber from app.RESERVATION where "
                    + "(? >= checkIn AND ? <= checkOut) OR"
                    + "(? < checkIn AND ? < checkOut AND ? > checkIn) OR"
                    + "(? > checkIn AND ? < checkOut AND ? > checkOut) OR"
                    + "(? < checkIn and ? > checkOut)"
            );
            getAllBookedRooms.setDate(1, startDate);
            getAllBookedRooms.setDate(2, endDate);
            getAllBookedRooms.setDate(3, startDate);
            getAllBookedRooms.setDate(4, endDate);
            getAllBookedRooms.setDate(5, endDate);
            getAllBookedRooms.setDate(6, startDate);
            getAllBookedRooms.setDate(7, startDate);
            getAllBookedRooms.setDate(8, endDate);
            getAllBookedRooms.setDate(9, startDate);
            getAllBookedRooms.setDate(10, endDate);

            rs = getAllBookedRooms.executeQuery();
            while (rs.next()) {
                roomNumbers.add(
                        rs.getString("roomNumber")
                );
            }
            rs.close();
            getAllBookedRooms.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();
        return roomNumbers;

    }

    public boolean isRoomAvailableThatMorning(Date startDate, Integer roomnum) {

        boolean available = true;
        openConnection();
        try {
            getAllReservation = conn.prepareStatement("select * from app.RESERVATION where "
                    + "checkOut = ? AND "
                   + " roomnumber =  ?" );
            getAllReservation.setDate(1, startDate);
            getAllReservation.setInt(2, roomnum);
            rs = getAllReservation.executeQuery();
            if (rs.next()) {
                available = false;
            }
            rs.close();
            getAllReservation.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();

        return available;

    }
    
    public boolean isRoomAvailableThatAfternoon(Date endDate, Integer roomnum) {
        boolean available = true;
        openConnection();
        try {
            getAllReservation = conn.prepareStatement("select * from app.RESERVATION where "
                    + "checkIn = ? AND "
                   + " roomnumber =  ?" );
            getAllReservation.setDate(1, endDate);
            getAllReservation.setInt(2, roomnum);
            rs = getAllReservation.executeQuery();
            rs = getAllReservation.executeQuery();
            if (rs.next()) {
                available = false;
            }
            rs.close();
            getAllReservation.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        closeConnection();

        return available;
    }

    public List<String> getRefCodes() {
        List<String> refCodes = new ArrayList<String>();
        for (Reservation r : this.getReservation()) {
            refCodes.add(r.getRefCode());
        }
        return refCodes;
    }
//Get days
//1. set a date or similar object to the start date (gregorian calendar)
//2. add it to list
//3. move the date to the next day and repeat until you reach departure date (plusDays)

    
     private java.util.Date localDateToUtilDate(LocalDate startDate) {

     List<String> breakfastDates = new ArrayList<String>();
     ReservationQuery resQ = new ReservationQuery();
        
     GregorianCalendar cal = new GregorianCalendar(
     startDate.getYear(), startDate.getMonthValue()-1, startDate.getDayOfMonth());
     java.util.Date date = cal.getTime();
     return date;


     }
   

     public String[] getDays() {
     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     }

     
}


   
